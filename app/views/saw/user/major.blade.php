@extends('saw.user.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert">
            @if(Session::has('flash_notice'))
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Alert!</strong> {{ Session::get('flash_notice') }}
                    </div>
                </div>
            @endif
        </div><!-- ./row -->

        <div class="row">
            <div class="col-lg-12">
                <h1>Penjurusan <small>Listing penjurusan</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('user/major') }}">Penjurusan</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Informasi User</div>
                    <div class="panel-body">
                        <p>Berikut ini adalah informasi umum tentang anda.</p>
                    </div>
                        <table class="table">
                            <tr>
                                <th>Nama</th>
                                <td>: {{{ $users[0]->name }}}</td>
                            </tr>
                            <tr>
                                <th>No Induk</th>
                                <td>: {{{ $users[0]->id_number }}}</td>
                            </tr>
                            <tr>
                                <th>Kelas</th>
                                <td>: VII {{{ $users[0]->class }}}</td>
                            </tr>

                            {{ Form::open(['url' => 'user/major', 'role' => 'form']) }}
                            <tr>
                                <th>Minat</th>
                                <td>    
                                    <input type="hidden" name="student_id" value="{{{ $users[0]->student_id }}}" />
                                    <input type="hidden" name="major_id" value="{{{ $users[0]->user_major_id }}}" />
                                    {{ Form::select('major', array( 
                                            '' => 'Pilih', 
                                            '1' => 'IPA', 
                                            '2' => 'IPS'
                                        ), $users[0]->major_id, array( 
                                            'class' => 'form-control', 
                                            'required' => 'required'
                                        )
                                    )}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><button type="submit" class="btn btn-primary">Simpan</button></td>
                            </tr>
                            {{ Form::close() }}
                        </table>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Informasi User</div>
                     <div class="panel-body">
                        <p>Hasil penentuan jurusan pihak sekolah.</p>
                    </div>
                    <div class="panel-footer">
                        @if($majors)
                            Selamat anda masuk jurusan <b>{{{ strtoupper($majors) }}}</b>
                            <div style="width: 15%">
                                <a href="{{ URL::to('user/print') }}"><button class="btn btn-primary btn-md btn-block"><i class="fa fa-print"></i>Print</button></a>
                            </div>
                        @else
                            <b>Jurusan anda masih belum diketahui</b>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- /.row -->

    </div><!-- ./page-wrapper -->
@stop