<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        @include('saw.user.layout.head')
    </head>
<body>
    <header>
        @include('saw.user.layout.header')
    </header>
    <article>
        <div id="wrapper">
            @yield('content')
        </div><!-- /#wrapper -->
    </article>

    <footer>
        @include('saw.user.layout.footer')
    </footer>
</body>
</html>