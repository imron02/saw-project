<div id="wrapper">
    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/home') }}">SAW Project</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                <li><a href="{{ URL::to('/user/major') }}"><i class="fa fa fa-users"></i> Penjurusan</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right navbar-user">
                <li class="dropdown user-dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo Auth::user()->name ?> <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ URL::to('password') }}"><i class="fa fa-key"></i> Rubah Password</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo URL::to('logout') ?>"><i class="fa fa-power-off"></i> Keluar</a></li>
                  </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>