{{-- Custom Script --}}
{{-- HTML::script('assets/js/custom.js') --}}
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/morris-0.4.3.min.js') }}
{{ HTML::script('assets/js/raphael-min.js') }}

{{-- DataTables --}}
{{ HTML::script('assets/js/dataTables/media/js/jquery.dataTables.min.js') }}
{{ HTML::script('assets/js/dataTables/media/js/dataTables.bootstrap.js') }}