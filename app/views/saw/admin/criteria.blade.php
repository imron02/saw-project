@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert"></div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Kriteria <small>Halaman Kriteria</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('/admin/criteria') }}">Kriteria</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-4 pull-right">
                <form id="criteria">
                <div class="input-group">
                    <input type="text" id="rowValue" class="form-control" pattern="^[a-zA-Z ]*$" required="required" />
                    <span class="input-group-btn"><button type="submit" class="btn btn-default" id="addCriteria">Tambah</button>
                </div>
                </form>
            </div>
        </div><!-- /.row -->

        <br />
        <div class="row">
            <div class="col-lg-12">
             <table id="field" class="table table-striped table-hover" width="100%"> 
                <thead> 
                <tr> 
                    <th width="5%">No</th> 
                    <th width="85%">Nama Kriteria</th>
                    <th width="10%"></th>
                </tr> 
                </thead> 
                <tbody>
                    <?php $i = 1; ?>
                     @foreach($datas as $data)
                        <tr> 
                            <td class="form_code">{{ $i++ }}</td> 
                            <td class="rowInserts"><input type="hidden" name="id" value="{{ $data->id }}" />{{ $data->name }}</td>
                            <td>
                                <a href='#'><i class='fa fa-edit editRow' data-toggle='modal' data-target='#editModal'></i></a>&nbsp;&nbsp;
                                <a href='#'><i class='fa fa-trash-o deleteRow' data-togle='modal' data-target='#deleteCriteriaModal'></i></a>
                            </td>
                        </tr> 
                    @endforeach <?php unset($datas); unset($data) ?>
                </tbody> 
                </table> 
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <!-- edit modal -->
                <div class="modal fade" id="editModal" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Edit Kriteria</h4>
                            </div>
                            <form id="criteria1">
                            <div class="modal-body">
                                <input type="hidden" id="codeValue" />
                                <input type="hidden" id="qValue" />
                                <input type="text" class="form-control" id="editValue" pattern="^[a-zA-Z ]*$" required="required" autofocus="autofocus"  />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Save changes" />
                            </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.modal -->
            </div>
        </div><!-- ./row -->

        <!-- delelte modal -->
         <div class="row">
            <div class="col-md-12">
                <!-- delete Modal -->
                <div class="modal fade" id="deleteCriteriaModal" tabindex="-1" role="dialog" aria-labelledby="deleteCriteriaLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
                            </div>
                            <div class="modal-body">
                                <p>Apakah anda yakin ingin tetap menghapus?</p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-danger delYes">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- ./modal -->
            </div>
        </div><!-- ./row -->
    </div><!-- /#page-wrapper -->
@stop