@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1>Siswa <small>Tambah, perbaharui, dan hapus data</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/students') }}">Siswa</a></li>
                    <li><a href="{{ URL::to('admin/students/create') }}">Tambah Siswa</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Add</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
        	{{ Form::open(['url' => 'admin/students', 'role' => 'form', 'class' => 'form-horizontal']) }}
				<table class="table table-striped studentTable">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor Induk</th>
							<th>Nama</th>
							<th>Kelas</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="form_code">1</td> 
							<td>
								{{ Form::text('idNumber[]', $value = null, $attributes = [
										'class' => 'form-control', 
										'pattern' => '\d+', 
										'required' => 'required', 
										'placeholder' => 'Nomor Induk..'
									]) 
								}}
							</td>
							<td>
								{{ Form::text('name[]', $value = null, $attributes = [
										'class' => 'form-control', 
										'pattern' => '^[A-z ]+$', 
										'required' => 'required', 
										'placeholder' => 'Nama..'
									]) 
								}}
							</td>
							<td>
								<div class="row">
									<div class="col-sm-2">VII</div>
									<div class="col-sm-10">
										{{ Form::select('class[]', [
												'' => 'Select', 
												'A' => 'A', 
												'B' => 'B',
												'C' => 'C',
												'D' => 'D',
												'E' => 'E',
												'F' => 'F',
												'G' => 'G',
												'H' => 'H',
												'I' => 'I',
											], '',
											[
												'class' => 'form-control', 'required' => 'required'
											]) 
										}}
									</div>
								</div>
							</td>
							<td><a href="#" class="delTrStudent"><i class="fa fa-times-circle"></i></a></td>
						</tr>
					</tbody>
				</table>

				<div class="btn-group">
				  	<button type="button" class="btn btn-info" id="addStudent">Tambah</button>
				  	<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			{{ Form::close() }}
        </div> <!-- /.row -->
    </div>
@stop