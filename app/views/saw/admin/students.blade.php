@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert">
            @if(Session::has('flash_notice'))
                <div class="col-md-12">
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Peringatan!</strong> {{ Session::get('flash_notice') }}
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Siswa <small>Tambah, perbaharui, dan hapus data</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/students') }}">Siswa</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-2 col-md-offset-4">
                <div class="pull-right">
                    <a href="{{ URL::to('admin/students/create') }}" title="Tambah Siswa">
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-plus"></i> Tambah Siswa
                        </button>
                    </a>
                </div>
            </div>
        </div><!-- /.row -->

        <br />
        <div class="studentContent">
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-horizontal" role="form">
                        <table id="studentTable" class="table table-hover table-striped table-condensed table-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="15%">Nomor Induk</th>
                                    <th width="62%">Nama</th>
                                    <th width="10%">Kelas</th>
                                    <th width="8%"></th>
                                </tr>
                            </thead>
                         
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td></td> 
                                        <td><input type="hidden" name="idVal" value="{{ $data->id }}">{{ $data->id_number }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>VII {{ $data->class }}</td>
                                        <td><a href="#"><i class="fa fa-edit editStudent" data-toggle="modal" data-target="#editStudent"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-times-circle delStudent"></i></a></td>
                                    </tr>
                                @endforeach <?php unset($datas); unset($data) ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>

            <!-- Edit Modal -->
            <div class="modal fade" id="editStudent" tabindex="-1" role="dialog" aria-labelledby="editStudentModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Edit Student</h4>
                        </div>
                        <form action="{{ URL::to('admin/students/update') }}" method="post" accept-charset="utf-8" role="form" class="form-signin" id="studentForm">
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="idNumber">Id Number</label>
                                        <input type="hidden" name="id" />
                                        <input type="text" class="form-control" name="idNumber" required="required" pattern="\d+" />
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" />
                                    </div>
                                    <div class="form-group">
                                        <label for="class">Class</label>
                                        {{ Form::select('class', [
                                                '' => 'Select', 
                                                'A' => 'A', 
                                                'B' => 'B',
                                                'C' => 'C',
                                                'D' => 'D',
                                                'E' => 'E',
                                                'F' => 'F',
                                                'G' => 'G',
                                                'H' => 'H',
                                                'I' => 'I',
                                            ], NULL,
                                            [
                                                'class' => 'form-control', 'required' => 'required'
                                            ]) 
                                        }}
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- delete Modal -->
            <div class="modal fade" id="delStudentModal" tabindex="-1" role="dialog" aria-labelledby="deleteStudentLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda yakin ingin tetap menghapus?</p>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                                <button type="button" class="btn btn-danger delYes">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- ./studentContent -->
    </div><!-- /#page-wrapper -->
@stop