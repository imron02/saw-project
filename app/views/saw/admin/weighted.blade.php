@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert"></div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Bobot <small>Halaman bobot</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/analize/weighted') }}">Bobot</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        {{ Form::open(['url' => 'admin/analize/weighted', 'role' => 'form']) }}
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Kriteria</th>
                                <th>Bobot</th>
                            </tr>
                        </thead>
                            <tbody>
                                <?php $i = 1;?>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ $i++ }} @if (!empty($data->id)) <input type="hidden" name="id[]" value="{{ $data->id }}" /> @endif</td>
                                        <td>{{ $data->name }} <input type="hidden" name="criteria_id[]" value="{{ $data->criteria_id }}" /></td>
                                        <td>
                                        {{ Form::select('weight[]',
                                        [
                                            '' => '-Select-',
                                            '0.05' => 'Rendah',
                                            '0.15' => 'cukup',
                                            '0.25' => 'Tinggi',
                                            '0.5' => 'Sangat Tinggi'
                                        ], (float)number_format($data->value, 2, '.', ''), ['class' => 'form-control', 'required' => 'required']) }}
                                    </td>
                                    </tr>
                                @endforeach <?php unset($datas); unset($data); ?>
                            </tbody>
                    </table>

                    <div class="btn-group">
                        <button type="submit" class="btn btn-info">Simpan</button>
                        <a href="{{ URL::to('admin/analize/normalized') }}" class="btn btn-primary">Selanjutnya</a>
                        <!-- <button class="btn btn-primary"><a href="{{ URL::to('admin/analize/normalized') }}">Selanjutnya</a></button> -->
                    </div>
                </div>
            </div><!-- /.row -->
        {{ Form::close() }}<!-- ./form -->
    </div>
@stop