@extends('saw.admin.layout.default')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1>Data <small>Masukkan, perbaharui, dan hapus data</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/data/2') }}">Semester 2</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#listNull" data-toggle="tab">Data Semester 2</a></li>
                    <li><a href="#dataUpdate_semester_2" data-toggle="tab">Perbaharui Data</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="listNull">
                        <br />
                        <div class="table-responsive">
                            <table class="table table table-hover table-striped list-data">
                                <thead>
                                    <tr>
                                        <th>Id Number</th>
                                        <th>Name</th>
                                        <th>Class</th>
                                        <th width="5%"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($students as $student)
                                        <tr>
                                            <td id="idVal" data-student-id="{{ $student->id }}">
                                                {{ $student->id_number }}
                                            </td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->class }}</td>
                                            <td>
                                                <a href="#">
                                                    <i class="fa fa-edit dataInsert" data-toggle="modal" data-target="#editNull"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach <?php unset($students); unset($student) ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- ./tab pane -->

                    <div class="tab-pane" id="dataUpdate_semester_2"></div>
                </div>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <!-- Edit Modal -->
                <div class="modal fade" id="editNull" tabindex="-1" role="dialog" aria-labelledby="editNullModal" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                            </div>
                            {{ Form::open(['url' => 'admin/data/2', 'role' => 'form', 'id' => 'dataForm']) }}
                                <div class="modal-body">
                                    <table class="table table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Nilai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($criterias as $criteria)
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="idNumber" />{{ $criteria->name }}
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="criteria[<?php echo strtolower($criteria->id); ?>]" placeholder="<?php echo $criteria->name; ?>.." pattern="\d+" maxlength="2" required="required" />
                                                    </td>
                                                </tr>
                                            @endforeach <?php unset($criterias); unset($criteria) ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div><!-- ./Modal -->
            </div>
        </div><!-- ./Row -->
    </div>
@stop