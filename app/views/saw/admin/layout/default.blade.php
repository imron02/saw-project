<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        @include('saw.admin.layout.head')
    </head>
<body>
    <header>
        @include('saw.admin.layout.header')
    </header>

    <article>
        <div id="wrapper">
            @yield('content')
        </div><!-- /#wrapper -->
    </article>

    <footer>
        <script type="text/template" id="ctemplate">
        <tr>
            <td class="form_code"></td> 
            <td class="rowInsert"></td> 
            <td>
                <a href='#'><i class='fa fa-edit editRow' data-toggle='modal' data-target='#editModal'></i></a>
                &nbsp;&nbsp;
                <a href='#'><i class='fa fa-trash-o deleteRow'></i></a>
            </td>
        </tr> 
        </script>

        <script id="rtemplate" type="text/template">
                <tr>
                    <td class="form_code"></td> 
                    <td><input type="text" name="value[]" pattern="([\>]\d{2})?(\d{2}[\-]\d{2})?" maxlength="5" required="required" class="form-control checkVal"></td>
                    <td>
                        <select name="ipaAttr[]" required="required" class="form-control">
                            <option selected="selected" value="">-Select-</option>
                            <option value="0.25">Rendah</option>
                            <option value="0.5">cukup</option>
                            <option value="0.75">Tinggi</option>
                            <option value="1">Sangat Tinggi</option>
                        </select>
                    </td>
                    <td>
                        <select name="ipsAttr[]" required="required" class="form-control">
                            <option selected="selected" value="">-Select-</option>
                            <option value="0.25">Rendah</option>
                            <option value="0.5">cukup</option>
                            <option value="0.75">Tinggi</option>
                            <option value="1">Sangat Tinggi</option>
                        </select>
                    </td>
                    <td><a href='#'><i class='fa fa-trash-o delRating'></i></a></td>
                </tr>
        </script>

        <script type="text/javascript">
                var criteriaPage = "{{ URL::to('admin/criteria') }}",
                        ratingPage = "{{ URL::to('admin/rating') }}",
                        studentPage = "{{ URL::to('admin/students')}}",
                        dataPage = "{{ URL::to('admin/data')}}",
                        weightedPage = "{{ URL::to('admin/analize/weighted')}}",
                        majorsPage = "{{ URL::to('admin/majors') }}";
        </script>

        @include('saw.admin.layout.footer')
    </footer>
</body>
</html>