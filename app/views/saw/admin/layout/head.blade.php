<title>SAW Project</title>
<meta http-equiv="Content-type" content="application/x-www-form-urlencoded; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Application Weighted Product Project">
<meta name="Imron Rosdiana" content="WP Project">

<!-- Css -->
{{ HTML::style('assets/css/bootstrap.min.css') }}
<link rel="alternate" href="{{ URL::to('assets/css/bootstrap.css.map') }}" charset="utf-8" />
{{ HTML::style('assets/css/bootstrap-theme.min.css') }}
<link rel="alternate" href="{{ URL::to('assets/css/bootstrap-theme.css.map') }}" charset="utf-8" />
{{ HTML::style('assets/css/font-awesome/css/font-awesome.min.css') }}
{{ HTML::style('assets/css/morris-0.4.3.min.css') }}
{{ HTML::style('assets/css/sb-admin.css') }}

<!-- DataTables -->
{{ HTML::style('assets/js/dataTables/media/css/jquery.dataTables.min.css') }}
{{ HTML::style('assets/js/dataTables/media/css/dataTables.bootstrap.css') }}
<style type="text/css" media="screen">
	label.error {
		color: red;
		font-weight: lighter;
	}

	a button, a:visited button, a:hover button, button a, button a:visited, button a:hover {
		text-decoration: none;
		color: #FFF;
	}

	@-moz-document url-prefix() {
	    .btn-group-fix {
	    	padding-bottom: 8px;
	    }
	}

	.dataTables_wrapper .dataTables_paginate .paginate_button {
		padding: 0.5em 0em;
		margin-left: -2px;
	}

	table.dataTable thead th {
		border: none;
	}	

	table.dataTable.no-footer, table.dataTable.thead {
	    border-bottom: none;
	}

	.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
	    background: #FFF;
	    border-color: #FFF
	}

	.dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus {
		background: #080808;
	}

	/*fix firefox responsive table*/
	@-moz-document url-prefix() {
	  	fieldset { display: table-cell; }
	}
</style>

<!-- Jquery -->
{{ HTML::script('assets/js/jquery-2.1.0.min.js') }}
<link rel="alternate" href="{{ URL::to('assets/js/jquery-2.1.0.min.map') }}" charset="utf-8" />