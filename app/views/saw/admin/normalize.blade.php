@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert"></div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Normalisa <small>Normalisa matriks</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/analize/normalized') }}">Normalisa</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table table-hover table-striped" id="normalizeTable">
                        <thead>
                            <tr>
                                <th>No. Induk</th>
                                <th>Nama</th>
                                @foreach ($datas[0]->criteria_name as $critera)
                                    <th>{{ $critera }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                                <tr>
                                    <td>{{ $data->id_number }}</td>
                                    <td>{{ $data->name }}</td>
                                    @foreach ($data->normalize as $values)
                                        <td class="dataValue">{{ $values }}</td>
                                    @endforeach
                                </tr>
                            @endforeach <?php unset($datas); unset($data) ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="btn-group">
                <a href="{{ URL::to('admin/analize/weighted') }}" class="btn btn-info">Kembali</a>
                <a href="{{ URL::to('admin/analize/ranking') }}" class="btn btn-primary">Selanjutnya</a>
            </div>
        </div><!-- ./row -->

    </div><!-- ./page-wrapper -->
@stop