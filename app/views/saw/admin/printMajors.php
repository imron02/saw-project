<!DOCTYPE html>
<html>
<head>
	<title>Penjurusan</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		body {
			font-family: 'Arial';
			font-weight: normal;
		}

		#header {
			padding-bottom: 50px;
		}

		img {
			float: left;
			width: 15%;
		}

		#title {
			text-align: center;
			padding-left: 3%;
			line-height: 1px;
		}

		h4 {
			font-weight: normal;
			font-size: 16px;
		}

		caption {
		  background: #ccc;
		  font-weight: bold;
		  font-size: 1.1em;
		}

		caption, th, td {
		  	padding: .2em .2em;
		  	border: 1px solid #000;
		}
	</style>
</head>
<body>
	<div id="header">
		<div id="title">
			<h2>PEMERINTAH KABUPATEN TANGERANG</h2>
			<h3>DINAS PENDIDIKAN DAN KEBUDAYAAN</h3>
			<h3>SMAN 28 Kab. Tangerang</h3>
			<h4>Jl. Raya Cisauk - LAPAN, Cisauk - Tangerang, Banten</h4>
		</div>
	</div>

	<table border="1" width="100%">
		<caption>JURUSAN IPA</caption>
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="20%">No. Induk</th>
                <th width="65%">Nama</th>
                <th width="10%">Jurusan</th>
            </tr>
        </thead>
        <tbody>
        	<?php $i = 1; foreach($ipa as $r): ?>
            <tr>
            	<td style="text-align: center"><?php echo $i++ ?></td>
            	<td><?php echo $r->id_number ?></td>
            	<td><?php echo $r->name ?></td>
            	<td><?php echo strtoupper($r->major_name) ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    <!-- break -->
	<div style="page-break-before: always;"></div>
    
	<table border="1" width="100%">
		<caption>JURUSAN IPS</caption>
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="20%">No. Induk</th>
                <th width="65%">Nama</th>
                <th width="10%">Jurusan</th>
            </tr>
        </thead>
        <tbody>
        	<?php $j = 1; foreach($ips as $r): ?>
            <tr>
            	<td style="text-align: center"><?php echo $j++ ?></td>
            	<td><?php echo $r->id_number ?></td>
            	<td><?php echo $r->name ?></td>
            	<td><?php echo strtoupper($r->major_name) ?></td>
            </tr>
            <?php endforeach; unset($ipa); unset($ips); unset($r) ?>
        </tbody>
    </table>
</body>
</html>