@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert"></div>

		 <div class="row">
            <div class="col-lg-12">
                <!-- if there are login errors, show them here -->
                <p>
                    @if(Session::has('flash_notice'))
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Peringatan!</strong> {{{ Session::get('flash_notice') }}}
                        </div>  
                    @endif
                </p>
            </div>
        </div>
	
        <div class="row">
            <div class="col-lg-12">
                <h1>Password <small>Rubah password</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('/password') }}">Password</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Rubah Password</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
        	<div class="col-sm-4">
	        	{{ Form::open(['url' => 'password', 'role' => 'form', 'id' => 'changePassword']) }}
	        		<div class="form-group">
						<label for="current">Password Lama</label>
						<input type="password" name="old" class="form-control" placeholder="Password lama.." required="required">
					</div>
					<div class="form-group">
						<label for="new">Password Baru</label>
						<input type="password" name="new" class="form-control" placeholder="Password baru.." required="required">
					</div>
					<div class="form-group">
						<label for="retype">Ketik Ulang Password Baru</label>
						<input type="password" name="renew" class="form-control" placeholder="Password baru.." required="required">
					</div>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<button type="reset" class="btn btn-danger">Batal</button>
	        	{{ Form::close() }}
        	</div>
        </div>

    </div><!-- /#page-wrapper -->
@stop