@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                @if(Session::has('flash_notice'))
                    <div class="alert alert-warning alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <strong>Warning!</strong> {{ Session::get('flash_notice') }}
                    </div>
                @endif
            </div>
        </div><!-- ./row -->
        <div class="row">
            <div class="col-lg-12">
                <h1>Ranking <small>Listing perankingan</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/analize/ranking') }}">Ranking</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table table-hover table-striped" id="rankingTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Induk</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Ranking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                                <tr>
                                    <td></td>
                                    <td>{{ $data->id_number }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>VII {{{ $data->class }}}</td>
                                    <td>{{ $data->ranking }}</td>
                                </tr>
                            @endforeach <?php unset($datas); unset($data) ?>
                        </tbody>
                    </table>
                </div>
                <div class="btn-group">
                    <a href="{{ URL::to('admin/analize/normalized') }}" class="btn btn-info">Kembali</a>
                    <button class="btn btn-primary majors" data-toggle="modal" data-target="#majors">
                        Selanjutnya
                    </button>
                </div>
            </div>
        </div><!-- ./row -->

        <div class="row">
            <div class="col-lg-12">
                <!-- Modal -->
                <div class="modal fade" id="majors" tabindex="-1" role="dialog" aria-labelledby="majorsLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        {{ Form::open(['url' => 'admin/majors', 'role' => 'form', 'class' => 'form-horizontal']) }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                        <div class="form-group">
                                            <label for="majors">Jumlah siswa IPA</label>
                                            <input type="text" name="majors" pattern="\d+" class="form-control" placeholder="Jumlah siswa.." required />
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div><!-- ./modal -->
            </div><!-- ./col-lg-12 -->
        </div>

    </div><!-- ./page-wrapper -->
@stop