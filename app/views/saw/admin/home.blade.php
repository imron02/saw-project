@extends('saw.admin.layout.default')
@section('content')
	<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <!-- if there are login errors, show them here -->
                <p>
                    @if(Session::has('flash_notice'))
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Peringatan!</strong> {{{ Session::get('flash_notice') }}}
                        </div>  
                    @endif
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Dashboard <small>Dashboard Page</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Dashboard</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Aplikasi Penentuan Penjurusan</div>
                    <div class="panel-body">
                        <p>Aplikasi ini adalah aplikasi yang dikhususkan untuk membantu setiap siswa/guru dalam hal menentukan jurusan bagi siswa. Aplikasi ini menggunakan metode SAW <i>Simple Additive Weighting</i> yang merupakan bagian dari Sistem Penunjang Keputusan (SPK).</p>
                    </div>
                </div>
            </div>
        </div><!-- ./row -->
    </div><!-- /#page-wrapper -->
@stop