@extends('saw.admin.layout.default')
<style type="text/css" media="screen">
	div.col-xs-6 {
		padding-left: 0px;
	}

	.dataTables_length, .dataTables_info {
		padding-left: 7%;
	}
</style>
@section('content')
	<div id="page-wrapper">
        <div class="row row-alert"></div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Penjurusan <small>Listing penjurusan</small></h1>
                <ol class="breadcrumb">
                    <li><a href="{{ URL::to('/home') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="{{ URL::to('admin/majors') }}">Penjurusan</a></li>
                    <li class="active"><i class="icon-file-alt"></i> Listing</li>
                </ol>
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-offset-10 col-md-2">
                <a href="{{ URL::to('admin/print') }}"><button class="btn btn-primary btn-md btn-block"><i class="fa fa-print"></i>Print</button></a>
            </div>
        </div><!-- /.row -->

        <br />

        <div class="row">
        	<div class="col-lg-12">
        		<div class="table-responsive">
                    <table class="table table table-hover table-striped" id="majorTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Induk</th>
                                <th>Nama</th>
                                <th>Ranking</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datas as $data)
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="hidden" name="idVal" value="{{ $data->id }}" />
                                        {{ $data->id_number }}
                                    </td>
                                    <td>{{ $data->name }}</td>
                                    <td>
                                        <input type="hidden" name="majorId" value="{{ $data->major_id }}" />
                                        {{ strtoupper($data->majors_name) }}
                                    </td>
                                    <td>
                                        <a href="#">
                                            <i class="fa fa-edit editMajor" data-toggle="modal" data-target="#editMajor"></i>
                                        </a>&nbsp;&nbsp;
                                        <a href="#"><i class="fa fa-times-circle delMajor" data-toggle="modal" data-target="#deleteMajor"></i></a>
                                    </td>
                                </tr>
                            @endforeach <?php unset($datas); unset($data); ?>
                        </tbody>
                    </table>
                </div>
        	</div>
        </div><!-- ./row -->

        <div class="row">
            <div class="col-md-12">
                <!-- Edit Modal -->
                <div class="modal fade" id="editMajor" tabindex="-1" role="dialog" aria-labelledby="editMajorModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Edit Penjurusan</h4>
                            </div>
                            <form action="{{ URL::to('admin/majors/update') }}" method="post" accept-charset="utf-8" role="form" class="form-horizontal" id="studentForm">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="idNumber" class="col-sm-4">Nomor Induk</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" name="id" />
                                            <p id="idNumber"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4">Nama</label>
                                        <div class="col-sm-8">
                                            <p id="name"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="class" class="col-sm-4">Jurusan</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="major" name="major">
                                                <option value="1">IPA</option>
                                                <option value="2">IPS</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- ./moda-body -->
                                <div class="modal-footer">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- ./modal -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- delete Modal -->
                <div class="modal fade" id="deleteMajor" tabindex="-1" role="dialog" aria-labelledby="deleteMajorLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
                            </div>
                            <div class="modal-body">
                                <p>Apakah anda yakin ingin tetap menghapus?</p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-danger delYes">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- ./page-wrapper -->
@stop