<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Application Weighted Product Project">
		<meta name="Imron Rosdiana" content="WP Project">

		<title>SAW Project</title>
		<!-- Css -->
		<?php echo HTML::style('assets/css/bootstrap.min.css') ?>
		<?php echo HTML::style('assets/css/bootstrap.css.map') ?>
		<?php echo HTML::style('assets/css/bootstrap-theme.min.css') ?>
		<?php echo HTML::style('assets/css/bootstrap-theme.css.map') ?>
		<?php echo HTML::style('assets/css/font-awesome/css/font-awesome.min.css') ?>

		<!-- custom style -->
		<style type="text/css" media="screen">
			.form-signin {
			    margin: 0 auto;
			    max-width: 330px;
			    padding: 15px;
			}
			body {
			    background-color: #EEEEEE;
			    height: 100%;
			    padding-top: 10%;
			}
			.navbar-default {
				background-color: #2c3e50;
				background-image: linear-gradient(to bottom,#2c3e50 0,#2c3e50 100%);
			}
			.navbar-default .navbar-brand {
				color: #FFF;
			}
			a.navbar-brand{
				color: #FFF;
				font-size: 1.5em;
				font-weight: bold;
			}
			.form-signin .form-signin-heading, .form-signin .checkbox {
			    margin-bottom: 10px;
			}
			.form-signin input[type="email"] {
			    border-bottom-left-radius: 0;
			    border-bottom-right-radius: 0;
			    margin-bottom: -1px;
			}
			.form-signin input[type="password"] {
			    border-top-left-radius: 0;
			    border-top-right-radius: 0;
			    margin-bottom: 10px;
			}
			.form-signin .form-control {
			    -moz-box-sizing: border-box;
			    font-size: 16px;
			    height: auto;
			    padding: 10px;
			    position: relative;
			}
			.form-signin .checkbox {
			    font-weight: normal;
			}
		</style>
	</head>
<body>
	<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo URL::to('/') ?>">SAW PROJECT</a>
            </div>
        </div>
    </nav>

	<div class="container">

		<?php echo Form::open(array('url' => '/login', 'class' => 'form-signin', 'role' => 'form')) ?>
			<h2 class="form-signin-heading">Please sign in</h2>
			<!-- if there are login errors, show them here -->
	        <p>
	        	<?php  if(Session::has('flash_notice')): ?>
	        		<div id="flash_notice"><?php echo Session::get('flash_notice') ?></div>
        		<?php endif; ?>
	        </p>
	        <input type="text" name="username" value="<?php echo Input::old('username') ?>" class="form-control" required="required" autofocus="autofocus" autocomplete="off" placeholder="Username" />
			<input name="password" class="form-control" placeholder="Password" required="required" type="password" />
			<label class="checkbox">
				<input name="remember" value="1" type="checkbox" /> Remember me
			</label>
			<input type="submit" value="Sign in" class="btn btn-lg btn-primary btn-block" />
		<?php echo Form::close() ?>
	</div>
</body>
</html>
