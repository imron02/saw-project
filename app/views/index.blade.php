<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Application Weighted Product Project">
        <meta name="Imron Rosdiana" content="WP Project">

    	<title>SAW Project</title>
    	<!-- Css -->
    	{{ HTML::style('assets/css/bootstrap.min.css') }}
    	{{ HTML::style('assets/css/bootstrap.css.map') }}
    	{{ HTML::style('assets/css/bootstrap-theme.min.css') }}
    	{{ HTML::style('assets/css/bootstrap-theme.css.map') }}
    	{{ HTML::style('assets/css/cover.css') }}
        {{ HTML::style('assets/css/font-awesome/css/font-awesome.min.css') }}
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <!-- IE8 support for HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            .navbar-default {
                background-color: #2c3e50;
                background-image: linear-gradient(to bottom,#2c3e50 0,#2c3e50 100%);
            }

            .navbar-default .navbar-brand {
                color: #ffffff;
            }

            .navbar-default .navbar-nav>li>a {
                color: #ffffff;
            }

            .navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus {
                color: #18bc9c;
                background-color: transparent;
            }

            .visi-misi {
                text-align: center;
            }
        </style>

  		<!-- Jquery -->
  		{{ HTML::script('assets/js/jquery-2.1.0.min.js') }}
    </head>
<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">SAW PROJECT</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Visi Misi</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Sejarah</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Kontak</a>
                    </li>
                    <li class="page-scroll">
                        <a href="{{ URL::to('/login') }}">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ HTML::image('assets/img/profile.png') }}
                    <div class="intro-text">
                        <span class="name">SMAN 28 KAB. TANGERANG</span>
                        <hr class="star-light">
                        <span class="skills">Aplikasi Penentuan Penjurusan</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>VISI &amp; MISI</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 visi-misi">
                    <h3>Visi</h3>
                    <p>“Terwujud  Sekolah yang unggul dalam  akhlaq mulia, prestasi dan mandiri”</p>
                    <h3>Misi</h3>
                    <p>
                        Membentuk insan yang bertaqwa,berbudi pekerti luhur,berkepribadian nasional dan berpikir global,<br />
                        Mengefektifkan kegiatan pembelajaran yang optimal, baik didalam maupun diluar kelas, <br />
                        Bekerja keras, disiplin, kreatif dan bertanggungjawab dalam mewujudkan prestasi akademik, olah raga, seni dan sastra, <br />
                        Meningkatkan semangat berkompetisi yang sehat, kreatif dan inovatif dalam bidang akademik, seni dan olah raga ditingkat intern maupun ekstern, <br />
                        Membeli berbagai keterampilan yang siap pakai, <br />
                        Peduli lingkungan dalam rangka mendukung perbaikan ekosistem dunia, <br />
                        Menjalin kerja sama dengan orang tua, masyarakat lingkungan sekolah dan lembaga terkait (stakeholder) atas dasar manajemen berbasis sekolah, <br />
                        Menghasilkan lulusan yang mampu mengikuti pendidikan lanjutan dan mampu bersaing di era globalisasi.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>About</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <p>SMA Negeri 28 Kab. Tangerang berdiri 19 Juni 2007 (sebelumnya bernama SMAN 28 Cisauk), berdasarkan surat keputusan Bupati Tangerang nomor 421/Kep.134-Huk/2007.</p>
                </div>
                <div class="col-lg-4">
                    <p>Sekolah menengah negeri di kawasan Cisauk yang akan menjawab tantangan modernisasi dengan imtaq, kualitas akademis dan mengembangkan jiwa entrepreneur bagi remaja masa depan yang mandiri.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contact Me</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p>
                        Jl.Raya Cisauk – LAPAN, Cisauk - Tangerang Banten<br />
                        Telpon (021)7562197, 32987122/085218042778<br />
                        Fax (021) 7562197<br />
                        e-mail info@sman28kab-tangerang.sch.id
                    </p>
                </div>
            </div>
        </div>
    </section>

    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>Jl.Raya Cisauk – LAPAN,
                            <br>Cisauk - Tangerang Banten</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li><a href="https://www.facebook.com/groups/216473945315/" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li><a href="http://www.sman28kab-tangerang.sch.id/web/" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>About SAW PROJECT</h3>
                        <p>Aplikasi yang dikhususkan untuk penentuan jurusan bagi siswa</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; <?php echo date("Y"); ?> - Imron Rosdiana
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    {{ HTML::script('assets/js/bootstrap.min.js') }}
    {{ HTML::script('assets/js/cover/classie.js') }}
    {{ HTML::script('assets/js/cover/cbpAnimatedHeader.js') }}
    {{ HTML::script('assets/js/cover/freelancer.js') }}
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
</body>
</html>
