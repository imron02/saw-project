<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Home Page Route
|--------------------------------------------------------------------------
|
| Here is the route for first acces or for access from root application.
| This can show the default first page.
|
*/
Route::get('/', function() {
	return View::make('index');
});

/*
|--------------------------------------------------------------------------
| Login Route
|--------------------------------------------------------------------------
|
| In here the user will be validated, whether the user has previously 
| registered. If the user has been registered then the user will go to the dashboard.
|
*/
Route::get('/login', array('before' => 'guest', function()
{
	return View::make('login');
}));

Route::post('/login', 'LoginController@validate');

/*
|--------------------------------------------------------------------------
| Validating Role
|--------------------------------------------------------------------------
|
| After user is valid, the controller will check the user has role owner, or user,
| if user have role admin, then the user will go to the admin page, or if user
| only have rule user, then the user will go to the user page.
|
*/
Route::get('/home', array('before' => 'auth', 'uses' => 'HomeController@index'));

/*
|--------------------------------------------------------------------------
| Page with role admin
|--------------------------------------------------------------------------
| Only owners will have access to routes within admin/ prefix.
|
*/
Route::when('admin/*', 'owner_role');
Route::group(array('prefix' => 'admin' ,'before' => 'auth'), function() {
	
	// Criteria Page 
	Route::resource('criteria', 'CriteriaController');

	// Students Page 
	Route::resource('students', 'StudentsController');
	Route::post('students/update', 'StudentsController@updates');
        
    // Data Page
    Route::get('data/show/{id}', 'DataController@show_data');
    Route::post('data/update', 'DataController@updates' );
    Route::get('data/{id}', 'DataController@index');
    Route::post('data/{id}', 'DataController@store');

    // For majors
	Route::resource('majors', 'MajorsController');
	Route::post('majors/update', 'MajorsController@updates');

    // printing page
	Route::get('print', 'MajorsController@print_majors');
});


/*
|--------------------------------------------------------------------------
| Core SAW Algorithm
|--------------------------------------------------------------------------
| This is a core SAW (Simple Additive Weighting). Here have many function 
| to calculate the ranking for all students.
|
*/
Route::group(array('prefix' => 'admin/analize', 'before' => 'auth'), function() {
	/*for page weighted*/
	Route::get('weighted', 'AnalizeController@weightedShow');
	Route::post('weighted', 'AnalizeController@weightedInsert');

	/*for normalized*/
	Route::get('normalized', 'AnalizeController@normalizeShow');

	/*for ranking*/
	Route::get('ranking', 'AnalizeController@rankingShow');
});

/*
|--------------------------------------------------------------------------
| User Access
|--------------------------------------------------------------------------
| This is a route for only user access
|
*/
Route::when('user/*', 'can_read');
Route::group(array('prefix' => 'user'), function()
{
	Route::get('major', 'UserMajorController@show');
	Route::post('major', 'UserMajorController@insert');

	Route::get('print', 'UserMajorController@print_majors');
});


/*
|--------------------------------------------------------------------------
| Logout function
|--------------------------------------------------------------------------
*/
Route::get('/logout', function()
{
    Auth::logout();
    return Redirect::to('/login')
    		->with('flash_notice', 'You are successfully logged out.');
});

/*
|--------------------------------------------------------------------------
| Change Password Function
|--------------------------------------------------------------------------
*/
Route::resource('password', 'PasswordController');