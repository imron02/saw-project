<?php
use Illuminate\Database\Migrations\Migration;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
	}

}

class UserTableSeeder extends Seeder
{
  
    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'name'     => 'Imron Rosdiana',
            'username' => 'imron02',
            'email'    => 'imron@rosdiana.com',
            'password' => Hash::make('password'), //insert your password in here
        ));
    }
}