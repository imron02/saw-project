<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMajor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_major', function(Blueprint $table)
		{
			$table->increments('id');
			$table->Integer('major_id')
			      ->unsigned();
			$table->Integer('student_id')
				  ->unsigned();
			$table->foreign('major_id')
				  ->references('id')->on('majors')
				  ->onDelete('cascade');
			$table->foreign('student_id')
				  ->references('id')->on('students')
				  ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_major');
	}

}
