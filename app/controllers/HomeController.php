<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		if(Entrust::hasRole('User')) {
			return View::make('saw.user.home');
		}
		else if(Entrust::hasRole('Owner')) {
			return View::make('saw.admin.home');
		}
		else {
			Auth::logout();
			return Redirect::to('/login')
				->with('flash_notice', 'You don\'t have access!');
		}
		
	}

}