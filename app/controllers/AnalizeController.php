<?php
class AnalizeController extends \BaseController 
{
	private $_model;
	private $_data;

	public function __construct()
	{
		$this->_model = new Analize;
	}

	public function weightedShow()
	{
		$criteria = DB::table('criteria')
			            ->leftJoin('weighting', 'criteria.id', '=', 'weighting.criteria_id')
			            ->select('weighting.id', 'criteria.id as criteria_id', 'criteria.name', 'weighting.value')
			            ->orderBy('criteria.id', 'ASC')
			            ->get();

		return View::make('saw.admin.weighted')->with('datas', $criteria);
	}

	public function weightedInsert()
	{
		$post = Input::all();
		$count_weighting = Analize::all();

		if (is_array($post) && (count($post['criteria_id']) == count($post['weight']))) {
			// if the data already exist
			if($count_weighting->count() > 0) {
				for($i = 0; $i < count($post['id']); $i++) {
	                $weighting = Analize::find((int)$post['id'][$i]);
	                $weighting->value = $post['weight'][$i];
	                $weighting->save();
                }
			} 
			// if data not exist
			else {
		        for($i = 0; $i < count($post['criteria_id']); $i++) {
		            // create new array
		            $newData[] = [
		                'criteria_id' => $post['criteria_id'][$i],
		                'value' => $post['weight'][$i],
		                'created_at' => date('Y-m-d H:i:s'), 
		                'updated_at' => date('Y-m-d H:i:s'),
		            ];
		        }
		        Analize::insert($newData);
			}
		}

		return Redirect::to('admin/analize/weighted');
	}

	public function normalizeShow()
	{	
		$normalize = DB::table('students')
						->join('data_semester_1 AS a', 'a.student_id', '=', 'students.id')
						->join('data_semester_2 AS b', 'b.student_id', '=', 'students.id')
						->join('criteria', function($join) {
							$join->on('a.criteria_id', '=', 'criteria.id');
							$join->on('b.criteria_id', '=', 'criteria.id');
						})
						->select(
							'students.id_number',
							'students.name',
							DB::raw('GROUP_CONCAT(criteria.name ORDER BY criteria.id) AS criteria_name'),
							DB::raw('GROUP_CONCAT(ROUND(normalize(a.value, b.value, criteria.id), 4) ORDER BY criteria.id) AS normalize')
						)
						->groupBy('students.id')
						->get();
		
        // explode array result from group_concat to array with index
        foreach ($normalize as $data) {
            $data->criteria_name = explode(',', $data->criteria_name);
            $data->normalize = explode(',', $data->normalize);
        }
		return View::make('saw.admin.normalize')->with('datas', $normalize);
	}

	public function ranking()
	{
		$ranking = DB::table('students')
					->join('data_semester_1 AS a', 'a.student_id', '=', 'students.id')
					->join('data_semester_2 AS b', 'b.student_id', '=', 'students.id')
					->join('criteria', function($join) {
						$join->on('a.criteria_id', '=', 'criteria.id');
						$join->on('b.criteria_id', '=', 'criteria.id');
					})
					->join('weighting', 'criteria.id', '=', 'weighting.criteria_id')
					->select(
						'students.id AS student_id', 'students.id_number', 'students.name', 'students.class',
						DB::raw('ROUND(sum(ranking(a.value, b.value, weighting.value, criteria.id)), 4) AS ranking')
					)
					->groupBy('students.id')
					->orderBy('ranking', 'DESC')
					->get();

        return $ranking;	
	}

	public function rankingShow()
	{
		$ranking = $this->ranking();
		return View::make('saw.admin.ranking')->with('datas', $ranking);
	}
}