<?php

class CriteriaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get all the criteria
		$criteria = Criteria::all();

		// load the view and pass the criteria
		return View::make('saw.admin.criteria')->with('datas', $criteria);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// get if request from ajax
		if (Request::ajax()) {
			$data = Input::all();
			try {
				// store data to database
				$criteria = new Criteria;
				$criteria->name = ucfirst($data['name']);
				$criteria->created_at = date('Y-m-d H:i:s');
				$criteria->updated_at = date('Y-m-d H:i:s');
				$criteria->save();
			} catch (Exception $e) {
				if ($e->errorInfo[1] == 1062) {
					return 0;
				}
			}
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update()
	{
		// get if request from ajax
		if (Request::ajax()) {
			// get ajax delete request
			$data = Input::all();

			// update data from database
			$criteria = Criteria::where('name', '=', $data['q']);
			$criteria->update(['name' => $data['name']]);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		// get if request from ajax
		if (Request::ajax()) {
			// get ajax delete request
			$data = Input::all();

			// destroy data from database
			try {
				$criteria = Criteria::where('name', '=', $data['name']);
				$criteria->delete();
			} catch (Exception $e) {
				// echo $e->getMessage();
				echo $e->getCode();
			}
		}
		
	}

}
