<?php

class UserMajorController extends \BaseController 
{
	private $_data = [];

	public function show()
	{
		$this->_data['users'] = DB::table('students')
							   ->where('users.id', Auth::user()->id)
							   ->join('users', 'students.id_number', '=', 'users.username')
							   ->leftJoin('user_major', 'students.id', '=', 'user_major.student_id')
							   ->select(
							   		'users.id AS user_id', 
							   		'students.id AS student_id', 
							   		'user_major.id AS user_major_id',
							   		'user_major.major_id AS major_id',
							   		'students.id_number', 
							   		'students.name', 
							   		'students.class')
							   ->get();

		$this->_data['majors'] = DB::table('student_major')
								->where('students.id_number', Auth::user()->username)
								->join('majors', 'student_major.major_id', '=', 'majors.id')
								->join('students', 'student_major.student_id', '=', 'students.id')
								->pluck('majors.name as majors_name');
		return View::make('saw.user.major', $this->_data);
	}

	public function insert()
	{
		$user = UserMajor::where('student_id', '=', Input::get('student_id'))->count();

		if($user) {
			$major = UserMajor::find(Input::get('major_id'));
			$major->major_id = (int)Input::get('major');
			$major->save();
		}
		else {
			$major = new UserMajor;
			$major->major_id = (int)Input::get('major');
			$major->student_id = (int)Input::get('student_id');
			$major->save();
		}

		return Redirect::to('user/major')->with('flash_notice', 'Selamat data berhasil di update');
	}

	public function print_majors()
	{
		$this->_data['ipa'] = DB::table('student_major')
			 ->join('majors', 'student_major.major_id', '=', 'majors.id')
			 ->join('students', 'student_major.student_id', '=', 'students.id')
			 ->select('student_major.id', 'students.id_number' , 'students.name', 'majors.name as major_name')
			 ->where('majors.name', 'ipa')
			 ->get();

		$this->_data['ips'] = DB::table('student_major')
			 ->join('majors', 'student_major.major_id', '=', 'majors.id')
			 ->join('students', 'student_major.student_id', '=', 'students.id')
			 ->select('student_major.id', 'students.id_number' , 'students.name', 'majors.name as major_name')
			 ->where('majors.name', 'ips')
			 ->get();

		$html = View::make('saw.admin.printMajors', $this->_data);
		$pdf = PDF::loadHTML($html);
		return $pdf->stream('saw_project.pdf');
	}
}