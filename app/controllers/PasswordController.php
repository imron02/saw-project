<?php

class PasswordController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$role = DB::table('users')
				->where('users.id', Auth::user()->id)
				->join('assigned_roles', 'users.id', '=', 'assigned_roles.user_id')
				->join('roles', 'assigned_roles.role_id', '=', 'roles.id')
				->pluck('roles.name');
				
		if(strtolower($role) == 'user') {
			return View::make('saw.user.password');
		} else {
			return View::make('saw.admin.password');
		}

		return App::abort(403);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new = (string)Input::get('new');
		$renew = (string)Input::get('renew');

		$user = DB::table('users')->where('username', Auth::user()->username)->first();
		if ( Hash::check(Input::get('old'), $user->password) && ($new == $renew) ) {
			DB::table('users')
				->where('username', Auth::user()->username)
				->update( array('password' => Hash::make($new)) );
				
			Auth::logout();
			return Redirect::to('login')
							->with('flash_notice', 'Password anda berhasil diubah');
		}
		else {
		    return Redirect::to('password')
							->with('flash_notice', 'Password yang anda masukkan salah, silahkan ulangi');
		}
	}

}
