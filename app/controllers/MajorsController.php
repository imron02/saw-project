<?php

use \AnalizeController;

class MajorsController extends \BaseController
{
	private $_data = array();

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$majors = DB::table('student_major')
				->join('majors', 'student_major.major_id', '=', 'majors.id')
				->join('students', 'student_major.student_id', '=', 'students.id')
				->select('student_major.id', 'student_major.major_id', 'students.id_number' , 'students.name', 'majors.name as majors_name')
				->get();
		return View::make('saw.admin.majors')->with('datas', $majors);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//call class AnalizeController from controllers/AnalizeController.php
		$analize = new \AnalizeController;

		//get post
		$post = Input::get('majors');
		$ranking = $analize->ranking();

		if(!empty($ranking)) {
			for($i = 0; $i < $post; $i++) {
				$ipa[] = [
					'major_id' 	 => 1,
	                'student_id' => $ranking[$i]->student_id,
	                'created_at' => date('Y-m-d H:i:s'), 
	                'updated_at' => date('Y-m-d H:i:s'),
	            ];
			}

			for($j = $post; $j < count($ranking); $j++) {
				$ips[] = [
					'major_id' 	 => 2,
	                'student_id' => $ranking[$j]->student_id,
	                'created_at' => date('Y-m-d H:i:s'), 
	                'updated_at' => date('Y-m-d H:i:s'),
	            ];
			}

			// joining array ipa and ips
			$join = array_merge($ipa, $ips);

			DB::table('student_major')->truncate();
			DB::table('student_major')->insert($join);
		}

		return Redirect::to('admin/majors');
	}

	/**
	 * Update a resource in storage.
	 *
	 */
	public function updates()
	{
		DB::table('student_major')
            ->where('id', Input::get('id'))
            ->update(array('major_id' => Input::get('major')));
        return Redirect::to('admin/majors');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Request::ajax()) {
			print_r(Input::all());
			try {
				DB::table('student_major')->where('id', '=', Input::get('key'))->delete();
			} catch (Exception $e) {
				echo $e->getCode();
			}
		}
	}

	public function print_majors()
	{
		$this->_data['ipa'] = DB::table('student_major')
			 ->join('majors', 'student_major.major_id', '=', 'majors.id')
			 ->join('students', 'student_major.student_id', '=', 'students.id')
			 ->select('student_major.id', 'students.id_number' , 'students.name', 'majors.name as major_name')
			 ->where('majors.name', 'ipa')
			 ->get();

		$this->_data['ips'] = DB::table('student_major')
			 ->join('majors', 'student_major.major_id', '=', 'majors.id')
			 ->join('students', 'student_major.student_id', '=', 'students.id')
			 ->select('student_major.id', 'students.id_number' , 'students.name', 'majors.name as major_name')
			 ->where('majors.name', 'ips')
			 ->get();

		$html = View::make('saw.admin.printMajors', $this->_data);
		$pdf = PDF::loadHTML($html);
		return $pdf->stream('saw_project.pdf');
	}
}