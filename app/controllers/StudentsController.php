<?php

class StudentsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$students = Students::all();
		return View::make('saw.admin.students')->with('datas', $students);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('saw.admin.studentsAdd');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		if (count($data['idNumber']) == count($data['name']) && count($data['name']) == count($data['class'])) {
			$i = 0;

			while ($i < count($data['idNumber'])) {
				echo $i.'<br />';
				$datas[] = [
	                'id_number' => (int)$data['idNumber'][$i],
	                'name' => $data['name'][$i],
	                'class' => $data['class'][$i],
	                'created_at' => date('Y-m-d H:i:s'), 
	                'updated_at' => date('Y-m-d H:i:s'),
	            ];
				$i++;
			}

			try {
				Students::insert($datas);
				$this->_insert_role($datas);
			} catch (Exception $e) {
				if ($e->errorInfo[1] == 1062) {
					return Redirect::to('admin/students')->with('flash_notice', 'Duplikat entri untuk nomor induk!');
				}
			}
		}
		
		return Redirect::to('admin/students');
	}

	/*
	* Store a role for new student with permission is user only.
	*
	*/
	private function _insert_role($datas)
	{
		$role = DB::table('roles')->where('name', 'User')->pluck('id');
		$users = array_map(function ($ar) {
			return $datas[] = [
				'name' => $ar['name'],
				'username' => $ar['id_number'],
				'password' =>  Hash::make('password')
			];
		}, $datas);

		try {
			User::insert($users);
			foreach ($users as $v) {
				$user = User::where('username','=', $v['username'])->first();
				$user->roles()->attach($role);
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function updates()
	{
		if(is_array(Input::all())) {
			$data = Input::all();
			$student = Students::find($data['id']);
			$student->id_number = (int)$data['idNumber'];
			$student->name = $data['name'];
			$student->class = $data['class'];
			$student->save();
		}

		return Redirect::to('admin/students');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Request::ajax()) {
			try {
				$key = Input::get('key');
				Students::destroy($key);
			} catch (Exception $e) {
				echo $e->getCode();
			}
		}
	}


}
