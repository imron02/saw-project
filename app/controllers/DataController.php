<?php

class DataController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		if((int)$id) {
			// if the id is 1 so get data from semester 1
			if($id == 1) {
				$data = [
	            'students' => DB::table('students')
	                                ->leftJoin('data_semester_1', 'data_semester_1.student_id', '=', 'students.id')
	                                ->whereNull('data_semester_1.student_id')
	                                ->select('students.*')
	                                ->get(),
	            'criterias' => Criteria::all(),
	            ];

	        	return View::make('saw.admin.data_semester_1', $data);
			} else {
				$data = [
	            'students' => DB::table('students')
	                                ->leftJoin('data_semester_2', 'data_semester_2.student_id', '=', 'students.id')
	                                ->whereNull('data_semester_2.student_id')
	                                ->select('students.*')
	                                ->get(),
	            'criterias' => Criteria::all(),
	            ];

	        	return View::make('saw.admin.data_semester_2', $data);
			}
		}
		return App::abort(403);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		if( (int)$id && is_array(Input::all()) ) {
			// if the id is 1 so get data from semester 1
			if($id == 1) {
				$datas = Input::all();
			
				unset($datas['_token']);
				foreach ($datas['criteria'] as $key => $v) {
					$newData[] = [
						'student_id' => $datas['idNumber'],
		                'criteria_id' => $key,
		                'value' => $v,
		                'created_at' => date('Y-m-d H:i:s'), 
		                'updated_at' => date('Y-m-d H:i:s'),
		            ];
				}
				DB::table('data_semester_1')->insert($newData);
			} else {
				$datas = Input::all();
			
				unset($datas['_token']);
				foreach ($datas['criteria'] as $key => $v) {
					$newData[] = [
						'student_id' => $datas['idNumber'],
		                'criteria_id' => $key,
		                'value' => $v,
		                'created_at' => date('Y-m-d H:i:s'), 
		                'updated_at' => date('Y-m-d H:i:s'),
		            ];
				}
				DB::table('data_semester_2')->insert($newData);
			}
		}
		return $this->index($id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show_data($id)
	{
		if( (int)$id ) {
			if($id == 1) {
		        $datas = DB::table('data_semester_1')
		                    ->join('criteria', 'data_semester_1.criteria_id', '=', 'criteria.id')
		                    ->join('students', 'data_semester_1.student_id', '=', 'students.id')
		                    ->select(
		                        'data_semester_1.student_id', 'students.id_number', 'students.name', 'students.class',
		                        DB::raw('GROUP_CONCAT(data_semester_1.criteria_id ORDER BY data_semester_1.criteria_id) AS criteria_id'),
		                        DB::raw('GROUP_CONCAT(criteria.name ORDER BY criteria.id) AS criteria_name'),
		                        DB::raw('GROUP_CONCAT(data_semester_1.value ORDER BY data_semester_1.criteria_id) AS value')
		                        )
		                    ->groupBy('student_id')
		                    ->get();

		        // xplode comma to array
				foreach ($datas as $data) {
					$data->criteria_id = explode(',', $data->criteria_id);
					$data->criteria_name = explode(',', $data->criteria_name);
					$data->value = explode(',', $data->value);
					$data->semester = $id;
				}

			} else {
				$datas = DB::table('data_semester_2')
		                    ->join('criteria', 'data_semester_2.criteria_id', '=', 'criteria.id')
		                    ->join('students', 'data_semester_2.student_id', '=', 'students.id')
		                    ->select(
		                        'data_semester_2.student_id', 'students.id_number', 'students.name', 'students.class',
		                        DB::raw('GROUP_CONCAT(data_semester_2.criteria_id ORDER BY data_semester_2.criteria_id) AS criteria_id'),
		                        DB::raw('GROUP_CONCAT(criteria.name ORDER BY criteria.id) AS criteria_name'),
		                        DB::raw('GROUP_CONCAT(data_semester_2.value ORDER BY data_semester_2.criteria_id) AS value')
		                        )
		                    ->groupBy('student_id')
		                    ->get();

		        // xplode comma to array
				foreach ($datas as $data) {
					$data->criteria_id = explode(',', $data->criteria_id);
					$data->criteria_name = explode(',', $data->criteria_name);
					$data->value = explode(',', $data->value);
					$data->semester = $id;
				}

			}

			return View::make('saw.admin.dataTab')->with('datas', $datas);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function updates()
	{
		$entity = Input::except('semester');
		$id = (int)Input::only('semester');

		if( is_array($entity) && (count($entity['criteria_id']) == count($entity['value'])) ) {
			if($id == 1) {
				$i = 0;
				// update data with two clause where
				while ($i < count($entity['criteria_id'])) {
					$data = DB::table('data_semester_1')
								->where('student_id', '=', $entity['idNumber'])
								->where('criteria_id', '=', $entity['criteria_id'][$i]);
					$data->update(['value' => $entity['value'][$i]]);
					$i++;
				}
			} else {
				$i = 0;
				// update data with two clause where
				while ($i < count($entity['criteria_id'])) {
					$data = DB::table('data_semester_2')
								->where('student_id', '=', $entity['idNumber'])
								->where('criteria_id', '=', $entity['criteria_id'][$i]);
					$data->update(['value' => $entity['value'][$i]]);
					$i++;
				}
			}
		}


		return Redirect::to('admin/data/'.$id);
	}

}
