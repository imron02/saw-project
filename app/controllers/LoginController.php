<?php 
class LoginController extends BaseController 
{
	
	public function validate() 
	{
        // set the remember me cookie if the user check the box
        $remember = (Input::has('remember')) ? true : false;

        // attempt to do the login
        $auth = Auth::attempt(
            [
                'username'  => strtolower(Input::get('username')),
                'password'  => Input::get('password')    
            ], $remember
        );
        if ($auth) {
            return Redirect::to('home');
        } else {
            // validation not successful, send back to form 
            return Redirect::to('/login')
                ->withInput(Input::except('password'))
                ->with('flash_notice', 'Kombinasi username/password anda salah!');
        }

	}
}
