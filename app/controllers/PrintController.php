<?php

use \AnalizeController;

class PrintController extends \BaseController
{
	private $_data;

	public function index($majors)
	{
		$analize = new \AnalizeController;

		$_data['post'] = $majors;
		$_data['rankings'] = $analize->ranking();

		$html = View::make('saw.admin.printMajors', $_data);
		$pdf = PDF::loadHTML($html);
		return $pdf->stream('saw_project.pdf');
		// return $pdf->stream('saw_project.pdf', array('Attachment'=>0));
	}
}