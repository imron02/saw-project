<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) 
		return Redirect::guest('/login')
				->with('flash_notice', 'You must be logged in to view this page!');
	// if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/home');
	// if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| Role Filter
|--------------------------------------------------------------------------
|
| The Role filter is the counterpart of the authentication filters as
| it simply checks that the current user is have role admin or user.
|
*/
Route::filter('owner_role', function()
{
    if (! Entrust::hasRole('Owner') ) // Checks the current user
    {
    	try {
	    	if(URL::previous() == URL::to('/'))
		    	return Redirect::to(URL::to('/home'))->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
		    else
		    	return Redirect::to(URL::previous())->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
    	} catch (Exception $e) {
    		return App::abort(403);
    	}
    }
});

Route::filter('can_read', function()
{
	if(!Entrust::can('can_read')) {
		try {
	    	if(URL::previous() == URL::to('/'))
		    	return Redirect::to(URL::to('/home'))->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
		    else
		    	return Redirect::to(URL::previous())->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
    	} catch (Exception $e) {
    		return App::abort(403);
    	}	
	}
});

Route::filter('can_edit', function()
{
	if(!Entrust::can('can_edit')) {
		try {
	    	if(URL::previous() == URL::to('/'))
		    	return Redirect::to(URL::to('/home'))->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
		    else
		    	return Redirect::to(URL::previous())->with('flash_notice', 'Kamu tidak mempunyai akses ke halaman ini!');
    	} catch (Exception $e) {
    		return App::abort(403);
    	}	
	}
});