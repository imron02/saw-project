<?php
class Helpers
{
    public static function xlog($data, $to_screen = true) 
    {
        if ($to_screen) {
            echo '<pre class="-debug prettyprint">';
            print_r($data);
            echo '</pre>' . "\n";
        }
    }
}