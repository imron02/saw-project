$(function () {

    "use strict";

    /*This is for check validity*/
    var isValid = null;

    /*Select first input to fucus*/
	$("#editModal").on("shown.bs.modal", function () {
        $("#editModal input:text").first().focus();
	});

    /* dynamical numbering function */
    function updateRowOrder() {
        $("td.form_code").each(function (i) {
            $(this).text(i + 1);
        });
    }

    function addField(data) {
        /* insert template */
        $($("#ctemplate").html()).appendTo("#field tbody:last");

        $(".rowInsert").append(data).attr("class", "rowInserts");
        updateRowOrder();
    }

    /* Trigger to button AddField if entered */
    $("#rowValue").keydown(function (event) {
        if (event.keyCode === 13) {
           $("#criteria").trigger("submit");
           $(this).val("");
        }
    });

    /* check clicked from delete or edit row alternative Criteria */
    $("#criteria").submit(function (event) {
        event.preventDefault();
        var rowValue = $("#rowValue").val().trim();

        /*Check validity, if false from trigger*/
        isValid = $(this)[0].checkValidity();
        if ((rowValue === "") || (false === isValid)) {
            return true;
        }

        /* send data with ajax */
        $.post(criteriaPage, { code: $(".form_code:last").text(), name: rowValue })
        .success(function (data) {
            if(data) {
                $(".row-alert").html("\
                    <div class='alert alert-warning'>\
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
                        <strong>Peringantan!</strong> Duplikat nama kriteria.\
                    </div>\
                ");
                return;
            }
            addField(rowValue);
        });
        $(this).val("");
    });

    $("#addCriteria").click(function (e) {
        $("#criteria").trigger("submit");
        $("#rowValue").val("");
    })

    $(document).on("click", ".deleteRow, .editRow", function (event) {
        /* if button with class #addRow clicked */
        if ($(this).is(".deleteRow")) {
            /* send with ajax */
            var tr     = $(this).closest("tr"),
                value  = tr.find(".rowInserts").text();
            $.ajax({
                url: criteriaPage + "/destroy",
                type: "DELETE",
                data: { name: value, _method: "delete" }
             })
            .success(function (data) {
                if (data == "23000") {
                    $(".row-alert").html("\
                        <div class='alert alert-warning'>\
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
                            <strong>Warnings!</strong> Cannot delete or update a parent row: a foreign key constraint fails.\
                        </div>\
                    ");
                    return event.preventDefault();
                }

                tr.remove();
                updateRowOrder();
            });
        } else {
            var id = $(this).closest("tr").find(".form_code").text(),
                text = $(this).closest("tr").find(".rowInserts").text();
            $("#editValue, #qValue").val(text);
            $("#codeValue").val(id);
        }
        return event.preventDefault();
    });

    /* Trigger to button .saveEdit if entered */
    $("#editValue").keydown(function (event) {
        if (event.keyCode === 13) {
            $("#criteria1").trigger("submit");
            $(this).val("");
            /* Hide Modal after save is clicked */
            $("#editModal").modal("hide");
        }

    });

    $("#criteria1").submit(function (event) {
        event.preventDefault();

        var id = $("#codeValue").val(),
            value = $("#editValue").val(),
            qValue = $("#qValue").val();
        
        /* send data with ajax */
         $.ajax({
            url: criteriaPage + "/update",
            type: "PUT",
            data: { q: qValue, name: value, _method: "put" }
         });

        /* change value table row */
        $("#field tbody tr").each(function () {
            if ($(this).find(".form_code").text() == id) {
                $(this).find(".rowInserts").text(value);
            }
        });
        
        /* Hide Modal after save is clicked */
        $("#editModal").modal("hide");

    });

    /*student Page*/
    /*variable for table student add for numbering*/
    var t = $("#studentTable").DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        /*order column 1 to ascending*/
        "order": [[ 1, "asc" ]],
        /*disable ordering column*/
        "aoColumnDefs" : [ {
            "bSortable" : false,
            "aTargets" : [ 0,4 ]
        } ],
    } );
    /*insert numbering table*/
    t.on( "order.dt search.dt", function () {
        t.column(0, {search:"applied", order:"applied"}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(document).on("click", "#addStudent, .delTrStudent", function (event) {
        var row = $(".studentTable tbody tr");
        /*add row if addStudent is clicked*/
        if ($(this).is("#addStudent")) {
            /*cloning tr if click add button*/
            $("tbody tr:first").clone().appendTo(".studentTable tbody");
            /*update numbering*/
            updateRowOrder();
        } else {
            /*don't delete if row less than 1*/
            if (row.length <= 1) {return event.preventDefault();}
            $(this).parents("tr").remove();
            updateRowOrder();
        }
        return event.preventDefault();
    });

    $(document).on("click", ".editStudent, .delStudent", function (event) {
        var attr = $(this).closest("tr"),
            id = attr.find("input[name='idVal']").val();

        if ($(this).is(".editStudent")) {
            /*initialitate array*/
            var idNumber = attr.find("td").eq(1).text(),
                name = attr.find("td").eq(2).text(),
                classVal = attr.find("td").eq(3).text().substring(4, 5);

            /*send all value to modal*/
            $("input[name='id']").val(id);
            $("input[name='idNumber']").val(idNumber);
            $("input[name='name']").val(name);
            $("select[name='class']").val(classVal);
        } else {
            /*showing modal*/
            $("#delStudentModal").modal("show");
            /*if button yes is clicked*/
            $(".delYes").click(function () {
                $.ajax({
                    url: studentPage + "/destroy",
                    type: "DELETE",
                    data: { key: id }
                 })
                .success(function (data) {
                    if (data == "23000") {
                        $(".row-alert").html("\
                            <div class='alert alert-warning'>\
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
                                <strong>Warnings!</strong> Cannot delete or update a parent row: a foreign key constraint fails.\
                            </div>\
                        ");
                        return event.preventDefault();
                    }

                    attr.remove();
                    /* Hide Modal after delete is succes */
                    $("#delStudentModal").modal("hide");
                });
            });
        }

        return event.preventDefault();
    });

    $(document).on("submit", "#studentForm", function (event) {
        /* Hide Modal after save is clicked */
        $("#editStudent").modal("hide");
    });

    /*Data Page*/
    $(document).on("click", ".dataInsert", function (event) {
        var idNumber = parseInt($(this).closest("tr").find("#idVal").data('student-id'));
        $("input[name='idNumber']").val(idNumber);
        $(this).closest("tr").addClass("rm" + idNumber);
        return event.preventDefault();
    });

    $('.list-data').dataTable();

    $(document).on("submit", "#dataForm", function (event) {
        /*hiding modal*/
        $("#editNull").modal("hide");
    });

    /*if dataUpdate is clicked*/
    $("a[href='#dataUpdate_semester_1']").click(function (event) {
        /*load content*/
        $("#dataUpdate_semester_1").load(dataPage + "/show/1");
        return event.preventDefault();
    });

    $("a[href='#dataUpdate_semester_2']").click(function (event) {
        /*load content*/
        $("#dataUpdate_semester_2").load(dataPage + "/show/2");
        return event.preventDefault();
    });

    $(document).on("click", "#editData", function (event) {
        event.preventDefault();
        /*getting this tr*/
        var tr = $(this).closest("tr").find(".dataValue");
        /*insert data value to form input*/
        $(tr).each(function (i) {
            $(".value").eq(i).val($(this).text());
        });
        /*insert id number to hidden input*/
        var idNumber = $(this).closest("tr").children('td:first').data('student-id');
        $("input[name='idNumber']").val(idNumber);
        /*showing modal*/
        $("#editDataModal").modal("show");
    });
    
    /*Normalize Page*/
    $("#normalizeTable").DataTable();

    /*Ranking Page*/

    /*variable for table student add for numbering*/
    var t = $("#rankingTable").DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        /*order column 1 to ascending*/
        "order": [[ 4, "desc" ]],
        /*disable ordering column*/
        "aoColumnDefs" : [ {
            "bSortable" : false,
            "aTargets" : [ 0 ]
        } ],
    } );
    /*insert numbering table*/
    t.on( "order.dt search.dt", function () {
        t.column(0, {search:"applied", order:"applied"}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    /*Majors Page*/
    /*variable for table student add for numbering*/
    var t = $("#majorTable").DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        /*order column 1 to ascending*/
        "order": [[ 3, "asc" ]],
        /*disable ordering column*/
        "aoColumnDefs" : [ {
            "bSortable" : false,
            "aTargets" : [ 0 ]
        } ],
    } );
    /*insert numbering table*/
    t.on( "order.dt search.dt", function () {
        t.column(0, {search:"applied", order:"applied"}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(document).on("click", ".editMajor, .delMajor", function (e) {
        // initialize data
        var attr = $(this).closest("tr"),
            id = attr.find("input[name='idVal']").val(),
            majorId = attr.find("input[name='majorId']").val(),
            idNumber = attr.find("td").eq(1).text(),
            name = attr.find("td").eq(2).text();

        if($(this).is(".editMajor")) {
            // send data to modal
            $("input[name='id']").val(id);
            $("#idNumber").text(idNumber);
            $("#name").text(name);
            $("#major").val(majorId);
        } else {
            /*if button yes is clicked*/
            $(".delYes").click(function () {
                $.ajax({
                    url: majorsPage + "/destroy",
                    type: "DELETE",
                    data: { key: id }
                 })
                .success(function (data) {
                    console.log(data);
                    attr.remove();
                    /* Hide Modal after delete is succes */
                    $("#deleteMajor").modal("hide");
                });
            });
        }
        return e.preventDefault();
    });

    /*Change Password*/
    $("#changePassword").submit(function (e) {
        if($("input[name='new']").val() != $("input[name='renew']").val()) {
            $(".row-alert").html("\
                <div class='alert alert-warning'>\
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
                    <strong>Peringantan!</strong> Password yang anda masukkan salah, silahkan ulangi.\
                </div>\
            ");
            $(".form-group").eq(1).addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-warning-sign form-control-feedback'></span>");
            $(".form-group").eq(2).addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-warning-sign form-control-feedback'></span>");
            return e.preventDefault();
        } else {
            return;
        }
    });
});