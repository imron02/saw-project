## SAW Project

SAW Project adalah aplikasi yang dibuat dan dikhususkan untuk melakukan perhitungan dalam penentuan jurusan (Sistem Penunjang Keputusan). SAW Project ini menggunakan metode SAW <i>Simple Additive Weighting</i> yang dikenal dengan perhitungan terbobot.

## Core Aplikasi
Aplikasi ini menggunakan:
<ol>
	<li>Laravel PHP Framework. Lihat disini [Laravel website](http://laravel.com)</li>
</ol>

### License

SAW Project adalah aplikasi <i>open-sourced</i> yang menggunakan Lisensi [GPL V3 license](https://www.gnu.org/copyleft/gpl.html)
