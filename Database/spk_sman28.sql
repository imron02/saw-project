-- MySQL dump 10.13  Distrib 5.6.17, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: spk_sman28
-- ------------------------------------------------------
-- Server version	5.6.17-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assigned_roles`
--

DROP TABLE IF EXISTS `assigned_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigned_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assigned_roles_users` (`user_id`),
  KEY `fk_assigned_roles_roles` (`role_id`),
  CONSTRAINT `fk_assigned_roles_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_assigned_roles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigned_roles`
--

LOCK TABLES `assigned_roles` WRITE;
/*!40000 ALTER TABLE `assigned_roles` DISABLE KEYS */;
INSERT INTO `assigned_roles` VALUES (8,3,15),(9,4,16),(10,5,16),(11,6,16),(12,7,16),(13,8,16),(14,9,16),(15,10,16),(16,11,16),(17,12,16);
/*!40000 ALTER TABLE `assigned_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `criteria`
--

DROP TABLE IF EXISTS `criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criteria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `criteria`
--

LOCK TABLES `criteria` WRITE;
/*!40000 ALTER TABLE `criteria` DISABLE KEYS */;
INSERT INTO `criteria` VALUES (8,'Matematika','2014-06-26 13:04:11','2014-06-26 13:04:11'),(9,'Biologi','2014-06-26 13:04:14','2014-06-26 13:04:14'),(10,'Fisika','2014-06-26 13:04:15','2014-06-26 13:04:15'),(11,'Kimia','2014-06-26 13:04:17','2014-06-26 13:04:17'),(12,'Ekonomi','2014-06-26 13:04:20','2014-06-26 13:04:20'),(13,'Geografi','2014-06-26 13:04:23','2014-06-26 13:04:23'),(15,'Sosiologi','2014-06-26 13:05:24','2014-06-26 13:05:24');
/*!40000 ALTER TABLE `criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_semester_1`
--

DROP TABLE IF EXISTS `data_semester_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_semester_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `criteria_id` int(10) unsigned NOT NULL,
  `value` tinyint(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_data_criteria` (`criteria_id`),
  KEY `fk_data_students` (`student_id`),
  CONSTRAINT `fk_data_semester_1_criteria` FOREIGN KEY (`criteria_id`) REFERENCES `criteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_data_semester_1_students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_semester_1`
--

LOCK TABLES `data_semester_1` WRITE;
/*!40000 ALTER TABLE `data_semester_1` DISABLE KEYS */;
INSERT INTO `data_semester_1` VALUES (64,16,8,80,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(65,16,9,88,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(66,16,10,87,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(67,16,11,86,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(68,16,12,80,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(69,16,13,88,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(70,16,15,90,'2014-06-26 13:51:46','2014-06-26 13:51:46'),(71,17,8,75,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(72,17,9,77,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(73,17,10,78,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(74,17,11,70,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(75,17,12,77,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(76,17,13,78,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(77,17,15,80,'2014-06-26 13:53:51','2014-06-26 13:53:51'),(78,18,8,80,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(79,18,9,80,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(80,18,10,90,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(81,18,11,70,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(82,18,12,77,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(83,18,13,78,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(84,18,15,86,'2014-06-26 13:54:27','2014-06-26 13:54:27'),(85,19,8,70,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(86,19,9,77,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(87,19,10,78,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(88,19,11,80,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(89,19,12,88,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(90,19,13,89,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(91,19,15,86,'2014-06-26 13:55:03','2014-06-26 13:55:03'),(92,20,8,70,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(93,20,9,70,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(94,20,10,70,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(95,20,11,60,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(96,20,12,60,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(97,20,13,67,'2014-06-26 14:28:40','2014-06-26 14:28:40'),(98,20,15,68,'2014-06-26 14:28:40','2014-06-26 14:28:40');
/*!40000 ALTER TABLE `data_semester_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_semester_2`
--

DROP TABLE IF EXISTS `data_semester_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_semester_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) unsigned NOT NULL,
  `criteria_id` int(10) unsigned NOT NULL,
  `value` tinyint(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_data_criteria` (`criteria_id`),
  KEY `fk_data_students` (`student_id`),
  CONSTRAINT `fk_data_semester_2_criteria` FOREIGN KEY (`criteria_id`) REFERENCES `criteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_data_semester_2_students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_semester_2`
--

LOCK TABLES `data_semester_2` WRITE;
/*!40000 ALTER TABLE `data_semester_2` DISABLE KEYS */;
INSERT INTO `data_semester_2` VALUES (1,16,8,70,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(2,16,9,78,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(3,16,10,77,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(4,16,11,76,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(5,16,12,70,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(6,16,13,78,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(7,16,15,80,'2014-06-26 14:37:53','2014-06-26 14:37:53'),(8,17,8,65,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(9,17,9,67,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(10,17,10,68,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(11,17,11,60,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(12,17,12,67,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(13,17,13,68,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(14,17,15,70,'2014-06-26 14:39:00','2014-06-26 14:39:00'),(15,18,8,70,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(16,18,9,70,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(17,18,10,80,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(18,18,11,60,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(19,18,12,67,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(20,18,13,68,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(21,18,15,76,'2014-06-26 14:40:04','2014-06-26 14:40:04'),(22,19,8,60,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(23,19,9,67,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(24,19,10,68,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(25,19,11,70,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(26,19,12,78,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(27,19,13,79,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(28,19,15,76,'2014-06-26 14:40:36','2014-06-26 14:40:36'),(29,20,8,60,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(30,20,9,60,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(31,20,10,60,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(32,20,11,50,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(33,20,12,50,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(34,20,13,57,'2014-06-26 14:41:08','2014-06-26 14:41:08'),(35,20,15,58,'2014-06-26 14:41:08','2014-06-26 14:41:08');
/*!40000 ALTER TABLE `data_semester_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `majors`
--

DROP TABLE IF EXISTS `majors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `majors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `majors`
--

LOCK TABLES `majors` WRITE;
/*!40000 ALTER TABLE `majors` DISABLE KEYS */;
INSERT INTO `majors` VALUES (1,'ipa','2014-05-30 02:26:31','2014-05-30 02:26:31'),(2,'ips','2014-05-30 02:26:31','2014-05-30 02:26:31');
/*!40000 ALTER TABLE `majors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_05_22_013722_create_majors_table',1),('2014_05_22_035033_create_student_major',1),('2014_05_22_041417_create_majors',1),('2014_05_28_003407_create_user_major_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_permission_role_roles` (`role_id`),
  KEY `fk_permission_role_permissions` (`permission_id`),
  CONSTRAINT `fk_permission_role_permissions` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_permission_role_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (10,7,16),(11,7,15),(12,8,15);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (7,'can_read','Can Read Data','2014-05-20 10:57:22','2014-05-20 10:57:22'),(8,'can_edit','Can Edit Data','2014-05-20 10:57:22','2014-05-20 10:57:22');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (15,'Owner','2014-05-20 10:57:22','2014-05-20 10:57:22'),(16,'User','2014-05-20 10:57:22','2014-05-20 10:57:22');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_major`
--

DROP TABLE IF EXISTS `student_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_major` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `major_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `student_major_major_id_foreign` (`major_id`),
  KEY `student_major_student_id_foreign` (`student_id`),
  CONSTRAINT `student_major_major_id_foreign` FOREIGN KEY (`major_id`) REFERENCES `majors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `student_major_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_major`
--

LOCK TABLES `student_major` WRITE;
/*!40000 ALTER TABLE `student_major` DISABLE KEYS */;
INSERT INTO `student_major` VALUES (1,1,16,'2014-06-30 21:24:01','2014-06-30 21:24:01'),(2,1,18,'2014-06-30 21:24:01','2014-06-30 21:24:01'),(3,1,19,'2014-06-30 21:24:01','2014-06-30 21:24:01'),(4,2,17,'2014-06-30 21:24:01','2014-06-30 21:24:01'),(5,2,20,'2014-06-30 21:24:01','2014-06-30 21:24:01');
/*!40000 ALTER TABLE `student_major` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_number` int(11) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `class` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idnumber` (`id_number`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (16,2010140419,'Imron Rosdiana','A','2014-06-26 13:06:32','2014-06-26 13:06:32'),(17,2010140420,'Firman Akbar Ramadhan','B','2014-06-26 13:06:32','2014-06-26 13:06:32'),(18,2010140421,'Asih Restari','C','2014-06-26 13:06:32','2014-06-26 13:06:32'),(19,2010140422,'Arif Hermansyah','D','2014-06-26 13:06:32','2014-06-26 13:06:32'),(20,2010140423,'Indri Gusti Ranti','A','2014-06-26 14:28:01','2014-06-30 06:28:02');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_major`
--

DROP TABLE IF EXISTS `user_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_major` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `major_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_major_student_id_foreign` (`student_id`),
  KEY `user_major_major` (`major_id`),
  CONSTRAINT `user_major_major` FOREIGN KEY (`major_id`) REFERENCES `majors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_major_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_major`
--

LOCK TABLES `user_major` WRITE;
/*!40000 ALTER TABLE `user_major` DISABLE KEYS */;
INSERT INTO `user_major` VALUES (1,17,2,'2014-06-30 21:23:16','2014-06-30 21:23:16');
/*!40000 ALTER TABLE `user_major` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `username` varchar(32) CHARACTER SET latin1 NOT NULL,
  `email` varchar(320) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `remember_token` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'Imron Rosdiana','imron02','imron02@gmail.com','$2y$10$1Jjuv48OiuglO3tzXI.09u3Tuz3jhIjegCNTq..DWknvOV/Zb6fMW','M2cOJ2gx93uVm9mKGJd9fRQHgfMhuII3oseAFdmOVDY1KweaiRrvHrKylWBv','2014-05-20 10:35:48','2014-06-27 12:14:48',NULL),(4,'Imron Rosdiana','2010140419','','$2y$10$w/xA1F71FWKp9j03yoVtweHKCORL.HDPbW3C/LAfAI3tn67POOqPW','6jIvZLawfEOQ0yEJaHxQbvk664Zc4Jlbsh0WhaWRG5fDDrBBkkdkd3LvC8R2','0000-00-00 00:00:00','2014-06-26 12:30:42',NULL),(5,'Arif Hermansyah','2010140420','','$2y$10$FrNcUkagzK/df4XuscSQyOw73iuOO0qu/Urt5R6UBcDvNm2tbreeS','QmzGXfDyoGHqBdWjLZ1k5zM47y20ionwgtBYalvjTSYbBwz3WLLA0NFKcVyb','0000-00-00 00:00:00','2014-06-30 21:23:21',NULL),(6,'Asih Restari','2010140421','','$2y$10$EgtMWKwoGx/.Gms5d0Ulfexrurksg4LcamXLodmIl6N/TrroPcoSi',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(7,'Firman Akbar Ramadhan','2010140422','','$2y$10$0aCAEiVFO0XPgSEoWYg4W.KXMj/v82jx.iBXUmPj/z/DNVrt.9SIu',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(8,'Indri Gusti Ranti','2010140423','','$2y$10$SvAfgsGD0XPU2MzGiJ.Gk.TRMuYCxQB.taL74T4df2rx2q5Sk5lmu',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(9,'Khaerunisa','2010140424','','$2y$10$RvGfnU5DiHwg.KNqlD7c8ez8MezXMZBSONJkqecdHnUaMMDen6bs.',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(10,'Ramadhan','2010140425','','$2y$10$FYJ3v/42sRdZ/.X7iGZzb.ZtaiyyGr2Mrv7zSLYlZz1fZU6hs1qw.',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(11,'Sarah Idayasa','2010140426','','$2y$10$TiDxU.qc.tn4WmhrL5BG.u9S.MCnvxrvUhb7Rn5bUIJiDmpiDOA5O',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(12,'Susi Susilawati','2010140427','','$2y$10$D.V14slLxNXYMoQFohCuwuwZjOIlZopucKks21evN7SKm8Oar4St6',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weighting`
--

DROP TABLE IF EXISTS `weighting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weighting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `criteria_id` int(10) unsigned DEFAULT NULL,
  `value` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_weighting_criteria` (`criteria_id`),
  CONSTRAINT `fk_weighting_criteria` FOREIGN KEY (`criteria_id`) REFERENCES `criteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weighting`
--

LOCK TABLES `weighting` WRITE;
/*!40000 ALTER TABLE `weighting` DISABLE KEYS */;
INSERT INTO `weighting` VALUES (8,8,0.5,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(9,9,0.25,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(10,10,0.25,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(11,11,0.25,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(12,12,0.15,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(13,13,0.15,'2014-06-26 14:51:29','2014-06-26 14:51:29'),(14,15,0.15,'2014-06-26 14:51:29','2014-06-26 14:51:29');
/*!40000 ALTER TABLE `weighting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'spk_sman28'
--
/*!50003 DROP FUNCTION IF EXISTS `normalize` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `normalize`(grade1 INT, grade2 INT,  criteria_key INT) RETURNS float
BEGIN
    DECLARE result FLOAT;
        SELECT grade1/max(a.value) + grade2/max(b.value)  INTO result
        FROM
            data_semester_1 a
        JOIN 
        	data_semester_2 b
        ON
        	a.student_id = b.student_id and a.criteria_id = b.criteria_id
        WHERE
            a.criteria_id = criteria_key and b.criteria_id = criteria_key;
		
RETURN result;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ranking` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ranking`(`grade1` INT, `grade2` INT, `weighting` FLOAT, `criteria_key` INT) RETURNS float
BEGIN
    DECLARE result FLOAT;	
        SELECT (grade1/max(a.value) + grade2/max(b.value))*weighting INTO result
        FROM
             data_semester_1 a
        JOIN 
        	data_semester_2 b
        ON
        	a.student_id = b.student_id and a.criteria_id = b.criteria_id
        WHERE
            a.criteria_id = criteria_key and b.criteria_id = criteria_key;
RETURN result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-04 22:32:27
